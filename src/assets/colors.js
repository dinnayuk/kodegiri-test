const COLORS = {
    /* FitAja colors */
    primary: "#509EE3",
    biruinh: "#043B73",
    secondary: '#4F4F4F',
    white: '#FFF',
    black: '#333333',
    light_grey: "#E0E0E0",
    dark_grey: '#828282',
    inline_error_color: '#b22222',
    placeholder_color: '#BDBDBD',
    overlay_color: 'rgba(0,0,0,0.7)',
    soft: "#7EC0EE",
    bold: "#03255b"
  };
  
  export default COLORS;
  