import {
  View
} from 'react-native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { NavigationContainer } from '@react-navigation/native';

import RootReducer from './redux/reducers/RootReducer';
import AppNavigator from './navigator/AppNavigator';

/** Register global state to redux store */
const store = createStore(RootReducer, applyMiddleware(thunk));

/** Base class for MovieDB Application. */
export default class App extends Component {

  render() {
      return (
          <Provider store={store} >
            <NavigationContainer>
              <AppNavigator />
            </NavigationContainer>
              
          </Provider>
      )
  }
}