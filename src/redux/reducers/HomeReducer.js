import { CONSTANTS } from "../../assets/data/Constants";

const initialState = {
    isLoading: false,
    isLoadingDetail: false,
    imageResponse: null,
    imageDetail: null
}

const Reducer = (state = initialState, action) => {
    if (action.type === CONSTANTS.REDUX_TYPE.IMAGE_RESPONSE){
        return { ...state, imageResponse: action.payload};
    } else if (action.type === CONSTANTS.REDUX_TYPE.IS_LOADING){
        return { ...state, isLoading: action.payload};
    } else if (action.type === CONSTANTS.REDUX_TYPE.IS_LOADING_DETAIL){
        return { ...state, isLoadingDetail: action.payload};
    } else if (action.type === CONSTANTS.REDUX_TYPE.IMAGE_DETAIL){
        return { ...state, imageDetail: action.payload};
    }
    return state;
}

export default Reducer;
