import { combineReducers } from 'redux';
import HomeReducer from './HomeReducer'

/** Combine all available reducers which will be accessible from all components */
export default combineReducers({
    home: HomeReducer
});
