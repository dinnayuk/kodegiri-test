import axios from 'axios';
import {CONSTANTS} from '../../assets/data/Constants';

const BASE_URL = 'https://api.unsplash.com';
const API_KEY = 'client_id=ghX9dF3ZzX6RU-GeqPLM21U5sQgVcrR13Rk-OAzq2G4'

export const updateImageResponse = response => ({
    type: CONSTANTS.REDUX_TYPE.IMAGE_RESPONSE,
    payload: response
});

export const updateImageDetail = response => ({
    type: CONSTANTS.REDUX_TYPE.IMAGE_DETAIL,
    payload: response
});

const isLoading = isLoading => ({
    type: CONSTANTS.REDUX_TYPE.IS_LOADING,
    payload: isLoading
})

const isLoadingDetail = isLoading => ({
    type: CONSTANTS.REDUX_TYPE.IS_LOADING_DETAIL,
    payload: isLoading
})

export function searchImage(keyword, page){
    const URL = `${BASE_URL}/search/photos?page=${page}&query=${keyword}&per_page=5&${API_KEY}`;
    return dispatch => {
        dispatch(isLoading(true))
        return axios.get(URL)
        .then(response => {
            console.log(response.data)
            dispatch(updateImageResponse(response.data))
            dispatch(isLoading(false))
        })
        .catch(error => dispatch(isLoading(false)))
    };
}

export function fetchPhotoDetail(id){
    const URL = `${BASE_URL}/photos/${id}/?${API_KEY}`
    return dispatch => {
        dispatch(isLoadingDetail(true))
        return axios.get(URL)
        .then(response => {
            dispatch(updateImageDetail(response.data))
            dispatch(isLoadingDetail(false))
        })
        .catch(error => dispatch(isLoading(false)))
    };
}