import React, { useEffect } from 'react'
import {View, Text, StyleSheet, ActivityIndicator, ScrollView, Image} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPhotoDetail } from '../../redux/actions/HomeActions';

import moment from 'moment'

const Details = ({route}) => {
    const dispatch = useDispatch();
    const {home} = useSelector(state => state);
    const {isLoadingDetail, imageDetail} = home;

    useEffect(() => {
        var {params} = route;
        var {item} = params;
        
        if (item){
            var id = item.id;
            dispatch(fetchPhotoDetail(id))
        }
    }, [])

    function renderLoading(){
        return (
            <View style={styles.container}>
                <ActivityIndicator size={'large'} />
            </View>
        )
    }

    function renderContent() {
        const ratio = imageDetail.width/imageDetail.height;
        console.log(moment(imageDetail.created_at).format('DD MM YYYY HH:mm'))

        return (
            <ScrollView style={{flex: 1, width: '100%'}} contentContainerStyle={{flexGrow: 1, padding: 16, alignItems: 'center'}}>
                <View style={{width: '100%', paddingTop: 20}}>
                    <Text style={styles.descriptionText}>{imageDetail.description}</Text>
                    <Image source={{uri: imageDetail.urls.regular}}
                            style={styles.image(ratio)}
                        />
                    <Text style={styles.contentText}>Tanggal Dibuat: {moment(imageDetail.created_at).format('DD/MM/YYYY HH:mm')}</Text>
                    <Text style={styles.contentText}>Tanggal Update: {moment(imageDetail.updated_at).format('DD/MM/YYYY HH:mm')}</Text>
                    <Text style={styles.contentText}>Sudah dilihat sebanyak: {imageDetail.views}</Text>
                    <Text style={styles.contentText}>Disukai {imageDetail.likes} kali</Text>
                    
                </View>
            </ScrollView>
        )
    }

    return (
        <View style={styles.container}>
            {isLoadingDetail ? renderLoading() : (imageDetail ? renderContent() : null)}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    descriptionText: {
        width: '100%',
        textAlign: 'left',
        fontSize: 20,
        fontWeight: 'bold'
    },
    image: (ratio) =>({
        width: '80%', height: undefined, aspectRatio: ratio, marginVertical: 20
    }),
    contentText: {
        fontSize: 16,
        color: '#474747',
        marginBottom: 8
    }
})

export default Details