import React, { useEffect, useState } from 'react'
import {View, Text, StyleSheet, TextInput, StatusBar, SafeAreaView, FlatList, Image, ActivityIndicator, Modal, TouchableOpacity} from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import {searchImage} from '../../redux/actions/HomeActions';
import IonIcons from 'react-native-vector-icons/Ionicons'

const Home = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const {home} = useSelector(state => state);
    const {imageResponse, isLoading} = home;

    let [keyword, setKeyword] = useState("");
    let [doSearch, setDoSearch] = useState(false);
    let [errorMessage, setErrorMessage] = useState("");
    let [page, setPage] = useState(1);
    let [imageList, setImageList] = useState([]);
    let [onReachEndMomentum, setOnReachEndMomentum] = useState(false);
    let [openPreview, setOpenPreview] = useState(false);
    let [selectedPreview, setSelectedPreview] = useState(null)

    useEffect(() => {
        if (imageResponse && imageResponse.results && imageResponse.results.length > 0){
            var list = [...imageList, ...imageResponse.results]
            setImageList(list)
        }
    }, [imageResponse])

    function doSearchImage(searchPage = 1) {
        if (keyword){
            setDoSearch(true)
            setErrorMessage("")
            dispatch(searchImage(keyword, searchPage))
        } else {
            setErrorMessage("Kata kunci pencarian tidak boleh kosong")
        }
    }

    function handleEnd(){
        setPage(page+1)
        doSearchImage(page+1)
    }

    function goToDetails(item){
        setOpenPreview(false)
        navigation.navigate("Details", {item})
    }

    function initialContent(){
        return (
            <View style={styles.contentContainer}>
                <Text style={{fontSize: 20, color: '#0A0708'}}>Cari Gambar</Text>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 12}}>
                    <TextInput
                        style={styles.textInput}
                        placeholder='Masukkan kata kunci pencarian'
                        placeholderTextColor={'#B1B1B1'}
                        onChangeText={text => setKeyword(text)}
                        returnKeyType='done'
                        onSubmitEditing={() => doSearchImage()}
                    />
                    <IonIcons.Button onPress={() => doSearchImage()} name="search-circle" size={45} color='#747474'
                        backgroundColor={'transparent'} iconStyle={{marginRight: 0}} style={{padding: 0}} />
                </View>
                {errorMessage ? <Text style={styles.textInlineError}>{errorMessage}</Text> : null}
            </View>
        )
    }

    function renderSearchResult() {
        return (
            <View style={{flex: 1, width: '100%', paddingTop: 16}}>
                <View style={{paddingHorizontal: 16, marginBottom: 20}}>
                    <Text style={{fontSize: 20, color: '#0A0708'}}>Cari Gambar</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 12}}>
                        <TextInput
                            value={keyword}
                            style={styles.textInput}
                            placeholder='Masukkan kata kunci pencarian'
                            placeholderTextColor={'#B1B1B1'}
                            onChangeText={text => setKeyword(text)}
                            returnKeyType='done'
                            onSubmitEditing={() => doSearchImage()}
                        />
                        <IonIcons.Button onPress={() => doSearchImage()} name="search-circle" size={45} color='#747474'
                            backgroundColor={'transparent'} iconStyle={{marginRight: 0}} style={{padding: 0}} />
                    </View>
                    {errorMessage ? <Text style={styles.textInlineError}>{errorMessage}</Text> : null}
                </View>
                {imageResponse ? <View style={{paddingHorizontal: 16, paddingVertical: 20, backgroundColor: '#BEAFC2'}}>
                    <Text style={{fontSize: 16}}>Berhasil menemukan {imageResponse.total} gambar dengan kata kunci <Text style={{fontStyle:'italic', fontWeight: 'bold'}}>'{keyword}'</Text></Text>
                    <Text style={{fontSize: 16, marginTop: 8}}>Jumlah halaman: {imageResponse.total_pages}</Text>
                </View> : null}

                {isLoading ? renderLoading() : (imageList ? renderList() : null)}
            </View>
        )
    }

    function renderLoading(){
        return (
            <View style={styles.container}>
                <ActivityIndicator size={'large'} />
            </View>
        )
    }

    function renderList() {
        return (
            <FlatList
                data={imageList}
                numColumns={2}
                renderItem={({ item, index }) => renderRow(item)}
                onEndReached={() => {
                    if(onReachEndMomentum){
                        handleEnd();
                        setOnReachEndMomentum(false)
                    }}}
                onEndReachedThreshold={0.6}
                onMomentumScrollBegin={() => {
                    setOnReachEndMomentum(true)
                }}
            />
        )
    }

    function renderRow(item){
        return (
            <TouchableOpacity style={{width: '50%', padding: 16}}
                onPress={() => {
                    setOpenPreview(true)
                    setSelectedPreview(item)
                }}
            >
                <Image source={{uri: item.urls.thumb}} style={{width: '100%', height: undefined, aspectRatio: 1/1, resizeMode: 'contain'}} />
                <TouchableOpacity style={styles.seeDetailButton} onPress={() => goToDetails(item)}>
                    <Text style={styles.seeDetailText}>Lihat Detail Gambar</Text>
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    function openPreviewModal(){
        const {width, height} = selectedPreview
        const aspectRatio = width/height;
        return (
            <Modal
                transparent={true}
                style={{flex: 1}}
                visible={true}>
                    <View style={styles.modalContainer}>
                        <View style={styles.previewContentContainer}>
                            <View style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                                <TouchableOpacity style={{alignItems: 'baseline', marginBottom: 12}} onPress={() => setOpenPreview(false)}>
                                    <IonIcons name="close" size={30}/>
                                </TouchableOpacity>                                
                            </View>
                            <Image source={{uri: selectedPreview.urls.regular}} style={{width: '100%', height: undefined, aspectRatio: aspectRatio, resizeMode: 'center'}}/>
                            <Text style={styles.previewDescription}>{selectedPreview.description}</Text>

                            <TouchableOpacity style={styles.seeDetailButton} onPress={() => goToDetails(selectedPreview)}>
                                <Text style={styles.seeDetailText}>Lihat Detail Gambar</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
        </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <SafeAreaView>
                <StatusBar backgroundColor="white" barStyle="dark-content" />
            </SafeAreaView>
            {!doSearch ? initialContent() : renderSearchResult()}
            {openPreview ? openPreviewModal() : null}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContainer: {
        width: '100%',
        height: '100%',
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)'
    },
    previewContentContainer: {width: '90%', backgroundColor: 'white',
        padding: 20, borderRadius: 20},
    contentContainer: {
        width: '80%'
    },
    previewDescription: {
        width: '100%',
        fontSize: 14,
        fontWeight: '500',
        marginVertical: 12,
    },
    textInput: {
        width: '80%',
        padding: 12,
        fontSize: 16,
        borderWidth: 1,
        borderColor: '#747474',
        borderRadius: 20,
        marginRight: 12
    },
    textInlineError: {fontSize: 12, color: 'red', fontStyle: 'italic', marginTop: 8},
    seeDetailButton: {
        backgroundColor: '#BEAFC2',
        padding: 10,
        borderRadius: 12
    },
    seeDetailText: {
        textAlign: 'center',
        fontSize: 16,
        color: '#281C2D'
    }
})

export default Home