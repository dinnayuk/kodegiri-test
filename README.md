# kodegiri-test


## Menjalankan Project

- Clone project melalui url: https://gitlab.com/dinnayuk/kodegiri-test.git

```
cd existing_repo
npm install
cd ios && pod install

```

- Untuk menjalankan project pada Android, menggunakan command line: react-native run-android

- Untuk menjalankan project pada iOS, menggunakan command line: react-native run-ios
